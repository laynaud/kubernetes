# kubernetes



## Pod, Replicaset, Deployment

Créer un pod avec des Resource Requests et Limits

Créer un namespace de noms limit.

Dans ce Namespace, créez un Pod nommé resource-checker de l’image httpd:alpine.

Le conteneur doit être nommé my-container.

Il doit demander 30m de CPU et être limité à 300m de CPU.

Il doit demander 30Mi de mémoire et être limité à 30Mi de mémoire.


```kubectl create namespace limit 

kubectl config set-context --current --namespace=limit
```

```cat <<EOF | kubectl apply -f -
kind: Pod
metadata:
  name: resource-checker
  namespace: limit
spec:
  containers:
  - name: my-container
    image: httpd:alpine
    resources:
      requests:
        memory: "30Mi"
        cpu: "30m"
      limits:
        memory: "30Mi"
        cpu: "300m"
EOF
```

## Services


```cat <<EOF | kubectl apply -f -
kind: ConfigMap
apiVersion: v1
metadata:
  name: nginx-conf
data:
  nginx.conf: |
    user nginx;
    worker_processes  3;
    error_log  /var/log/nginx/error.log;
    events {
      worker_connections  10240;
    }
    http {
      server {
          listen       80;
          server_name  _;
          root   /usr/share/nginx/html;
          index  index.html index.htm;

          location /europe {
              alias /usr/share/nginx/html/;
              index  index.html index.html;
          }
          location /asia {
              alias /usr/share/nginx/html/;
              index  index.html index.html;
          }
          location / {
              root   /usr/share/nginx/html;
              index  index.html index.htm;
          }
      }
    }

---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: europe
  name: europe
spec:
  progressDeadlineSeconds: 600
  replicas: 2
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      app: europe
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: europe
    spec:
      containers:
      - image: nginx:1.21.5-alpine
        imagePullPolicy: IfNotPresent
        name: c
        resources: {}
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
        volumeMounts:
        - mountPath: /usr/share/nginx/html
          name: html
        - mountPath: /etc/nginx
          name: nginx-conf
          readOnly: true
      dnsPolicy: ClusterFirst
      initContainers:
      - command:
        - sh
        - -c
        - echo 'hello, you reached EUROPE' > /html/index.html
        image: busybox:1.28
        imagePullPolicy: IfNotPresent
        name: init-container
        resources: {}
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
        volumeMounts:
        - mountPath: /html
          name: html
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: {}
      terminationGracePeriodSeconds: 30
      volumes:
      - emptyDir: {}
        name: html
      - configMap:
          defaultMode: 420
          items:
          - key: nginx.conf
            path: nginx.conf
          name: nginx-conf
        name: nginx-conf
ports:
    - protocol: TCP
      port: 80
      targetPort: 30291
  type: LoadBalancer
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: asia
  name: asia
spec:
  progressDeadlineSeconds: 600
  replicas: 2
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      app: asia
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: asia
    spec:
      containers:
      - image: nginx:1.21.5-alpine
        imagePullPolicy: IfNotPresent
        name: c
        resources: {}
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
        volumeMounts:
        - mountPath: /usr/share/nginx/html
          name: html
        - mountPath: /etc/nginx
          name: nginx-conf
          readOnly: true
      dnsPolicy: ClusterFirst
      initContainers:
      - command:
        - sh
        - -c
        - echo 'hello, you reached ASIA' > /html/index.html
        image: busybox:1.28
        imagePullPolicy: IfNotPresent
        name: init-container
        resources: {}
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
        volumeMounts:
        - mountPath: /html
          name: html
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: {}
      terminationGracePeriodSeconds: 30
      volumes:
      - emptyDir: {}
        name: html
      - configMap:
          defaultMode: 420
          items:
          - key: nginx.conf
            path: nginx.conf
          name: nginx-conf
        name: nginx-conf
ports:
    - protocol: TCP
      port: 80
      targetPort: 30292
  type: LoadBalancer
EOF
```
```curl http://172.17.0.15:30292

```